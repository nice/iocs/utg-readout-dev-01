# -----------------------------------------------------------------------------
# Detector Readout DEV-01
# -----------------------------------------------------------------------------
require detector_readout
require stream
require mrfioc2

# -----------------------------------------------------------------------------
# Environment settings
# -----------------------------------------------------------------------------

epicsEnvSet("TOP", "$(E3_CMD_TOP)/..")

epicsEnvSet("IOC",          "Utg-Ymir:sc-IOC-01")
epicsEnvSet("SYS",          "Utg-Ymir:")
epicsEnvSet("EVR",          "EVR-09")
epicsEnvSet("DEV",          "TS-$(EVR)")
epicsEnvSet("DEVSYS",       "$(SYS)TS")
epicsEnvSet("ROB",          "RO2")
epicsEnvSet("SYSPV",        "$(SYS)$(DEV)")
epicsEnvSet("PCIID",        "5:0.0")
epicsEnvSet("MRF_HW_DB",    "evr-pcie-300dc-univ.db")



# -----------------------------------------------------------------------------
# e3 Common databases, autosave, etc.
# -----------------------------------------------------------------------------
# E3 Common databases
iocshLoad("$(essioc_DIR)/essioc.iocsh")

# -----------------------------------------------------------------------------
# EVR PCI setup
# -----------------------------------------------------------------------------
iocshLoad("$(mrfioc2_DIR)/evr.iocsh", "EVRDB=$(MRF_HW_DB), P=$(SYSPV), OBJ=$(EVR), PCIID=$(PCIID)")
iocshLoad("$(mrfioc2_DIR)/evrevt.iocsh", "P=$(SYSPV), OBJ=$(EVR)")


# -----------------------------------------------------------------------------
############# ----------- Detector Readout Interface ------------ #############
# -----------------------------------------------------------------------------
epicsEnvSet("EPICS_CMDS",   "/opt/iocs")
epicsEnvSet("MASTER",       "$(EPICS_CMDS)/$(IOCNAME)/dgro_adc")
epicsEnvSet("PARAM_PARSE",  "$(MASTER)/det_param_gen/src/param_parse.py")
epicsEnvSet("OUTPUT",       "det_param_gen/output/EPICS")
epicsEnvSet("STREAM_PROTOCOL_PATH", "$(detector_readout_DIR)/db")
epicsEnvSet("PROTO",                "ics-dg-udp.proto")
epicsEnvSet("DET_CLK_RST_EVT",      "15")
epicsEnvSet("DET_RST_EVT",          "15")
epicsEnvSet("SYNC_EVNT_LETTER",     "EvtF")
epicsEnvSet("SYNC_TRIG_EVT",        "16")
epicsEnvSet("NANO_DELTA",           "1000000000")

epicsEnvSet("DestIP1", "192.168.50.6")
epicsEnvSet("DestIP2", "192.168.50.4")

epicsEnvSet("SrcIP",    "192.168.50.12")

epicsEnvSet("DestPort", "65535")
epicsEnvSet("SrcPort1", "65534")
epicsEnvSet("SrcPort2", "65533")

drvAsynIPPortConfigure ("UDP1","$(DestIP1):$(DestPort):$(SrcPort1) UDP")
drvAsynIPPortConfigure ("UDP2","$(DestIP2):$(DestPort):$(SrcPort2) UDP")

# -----------------------------------------------------------------------------
# Run the db generate script
# -----------------------------------------------------------------------------
# The generating python script is remove from st.cmd, it will be run independentlly
# system "/usr/bin/python3 $(PARAM_PARSE) $(MASTER)/param_def/"

dbLoadRecords("$(detector_readout_DIR)/db/evr_detector_controls.db", "SYS=$(DEVSYS), TSEVT=$(SYNC_EVNT_LETTER), SYNC-EVNT=$(SYNC_TRIG_EVT), DEV=$(ROB), EVR=$(EVR)")
iocshLoad("$(detector_readout_DIR)/bm_params_mst.cmd", "DEV=$(ROB), COM=UDP2, SYS=$(DEVSYS), PROTO=$(PROTO), EVR=$(EVR)")

iocInit()

iocshLoad("$(mrfioc2_DIR)/evr.r.iocsh", "P=$(SYSPV), OBJ=$(EVR)")


# Connect prescaler reset to event $(DET_CLK_RST_EVT)
dbpf $(SYSPV):Evt-ResetPS-SP $(DET_CLK_RST_EVT)

# Connect the counters to events
dbpf $(SYSPV):EvtE-SP 14
dbpf $(SYSPV):EvtE-SP.OUT "@OBJ=$(EVR),Code=14"
dbpf $(SYSPV):EvtF-SP $(DET_CLK_RST_EVT)
dbpf $(SYSPV):EvtF-SP.OUT "@OBJ=$(EVR),Code=$(DET_CLK_RST_EVT)"
dbpf $(SYSPV):EvtG-SP $(SYNC_TRIG_EVT)
dbpf $(SYSPV):EvtG-SP.OUT "@OBJ=$(EVR),Code=$(SYNC_TRIG_EVT)"
#dbpf $(SYSPV):EvtG-SP.OUT "@OBJ=$(EVR),Code=15"


# Map pulser 9 to event code SYNC_TRIG_EVT
dbpf $(SYSPV):DlyGen-9-Evt-Trig0-SP $(SYNC_TRIG_EVT)
dbpf $(SYSPV):DlyGen-9-Width-SP 10

# Set up Prescaler 0
dbpf $(SYSPV):PS-0-Div-SP 2

# Connect FP10 to PS0
dbpf $(SYSPV):Out-RB10-Ena-SP 1
dbpf $(SYSPV):Out-RB10-Src-SP 40

# Connect FP11 to Pulser 9
dbpf $(SYSPV):Out-RB11-Ena-SP 1
dbpf $(SYSPV):Out-RB11-Src-SP 9


# Connect FP12 to PS0
dbpf $(SYSPV):Out-RB12-Ena-SP 1
dbpf $(SYSPV):Out-RB12-Src-SP 40

# Connect FP09 to PS0
dbpf $(SYSPV):Out-RB09-Ena-SP 1
dbpf $(SYSPV):Out-RB09-Src-SP 40

# Connect FP13 to Pulser 9
dbpf $(SYSPV):Out-RB13-Ena-SP 1
dbpf $(SYSPV):Out-RB13-Src-SP 9

# Map pulser 7 to event code 14
dbpf $(SYSPV):DlyGen-7-Evt-Trig0-SP 14
## --- Map pulser 7 (which triggers sequencer) to event 14 to model meta data trigger) ---- ##
dbpf $(SYSPV):DlyGen-7-Evt-Trig0-SP 14
dbpf $(SYSPV):DlyGen-7-Width-SP 10

# Connect FP2 to Pulser 9
dbpf $(SYSPV):Out-RB02-Ena-SP 1
dbpf $(SYSPV):Out-RB02-Src-SP 9

# Connect FP3 to Pulser 9
dbpf $(SYSPV):Out-RB03-Ena-SP 1
dbpf $(SYSPV):Out-RB03-Src-SP 9

######## load the sync sequence ######
dbpf $(SYSPV):SoftSeq-0-Disable-Cmd 1
dbpf $(SYSPV):SoftSeq-0-Unload-Cmd 1
dbpf $(SYSPV):SoftSeq-0-Load-Cmd 1

#Use ticks
dbpf $(SYSPV):SoftSeq-0-TsResolution-Sel  "0"
dbpf $(SYSPV):SoftSeq-0-Commit-Cmd 1

#connect the sequence to software trigger
#dbpf $(SYSPV):SoftSeq-0-TrigSrc-Scale-Sel "Software"

#connect the sequence to software trigger
dbpf $(SYSPV):SoftSeq-0-TrigSrc-Pulse-Sel "Pulser 7"

#dbpf $(SYSPV):SoftSeq-0-RunMode-Sel "Single"

#add sequence events and corresponding tick lists
#system "/bin/bash /epics/iocs/cmds/hzb-v20-evr-02-cmd/evr_seq_sync.sh"

#perform sync one next event 125
#dbpf $(SYSPV):SoftSeq-0-Enable-Cmd 1

#dbpf $(SYSPV):syncTrigEvt-SP $(SYNC_TRIG_EVT)
dbpf $(SYSPV):FracNsecDelta-SP 88052500

dbpf $(DEVSYS)-$(ROB):BM-MST-IP-SRC-ADDR-SP $(SrcIP)
dbpf $(DEVSYS)-$(ROB):BM-MST-UDP-SRC-PORT-SP $(SrcPort2)
dbpf $(DEVSYS)-$(ROB):BM-MST-IP-DST-ADDR-SP $(DestIP2)
dbpf $(DEVSYS)-$(ROB):BM-MST-UDP-DST-PORT-SP $(DestPort)

dbpf $(DEVSYS)-$(ROB):BM-MST-CLK-SEL-SP "External"
#EOF

